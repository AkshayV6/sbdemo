/**
 * 
 */
package com.example.SBDemo.assignment1;

import org.springframework.stereotype.Component;

/**
 * @author AkshayV Lenovo
 *
 */
@Component
public class BankDetailsBean {

	private String bankName;
	private String bankAddess;
	
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAddess() {
		return bankAddess;
	}
	public void setBankAddess(String bankAddess) {
		this.bankAddess = bankAddess;
	}
	
}
