/**
 * 
 */
package com.example.SBDemo.assignment1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 	Assignment-1:

	Create a Spring Boot Application based on the below requirement:
	a.	The application should support two restful URLs.
	b.	The first restful URL should display the bank Name as a String
	c.	The second restful URL should display the bank Address as a String
	d.	We would also be interested to see the list of all the default beans 
			that were created by Spring Boot automatically in the console.
	e.	Test the application by executing and viewing the console 
			to see the list of beans and also test both the URLs to verify that 
			they return and display data as expected.
 
 * 
 * @author AkshayV Lenovo
 *
 */
@RestController
@RequestMapping("demo")
public class BankDetaillsController {

	private BankDetailsBean bank = new BankDetailsBean();
	
	@GetMapping("bankName")
	public String showBankName() {
		bank.setBankName("ICICIBank");
		System.out.println("BankName : " + bank.getBankName());
		return bank.getBankName();
	}
	
	@GetMapping("bankAddress")
	public String showBankAddress() {
		bank.setBankAddess("BKC");
		System.out.println("BankAddess : " + bank.getBankAddess());
		return bank.getBankAddess();
	}
}
